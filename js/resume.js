//resume.js
// Fonction pour basculer l'affichage de la description
function toggleDescription(elementId) {
    var descriptions = document.getElementsByClassName('description');

    for (var i = 0; i < descriptions.length; i++) {
        // Retirez la classe de toutes les descriptions pour réinitialiser l'animation
        descriptions[i].classList.remove('fadeIn');
        descriptions[i].style.display = 'none';
    }

    var description = document.getElementById(elementId);
    if (description.style.display === 'none' || description.style.display === '') {
        // Ajoutez la classe pour activer l'animation lorsqu'elle est affichée
        description.classList.add('fadeIn');
        description.style.display = 'block';
    } else {
        description.style.display = 'none';
    }
}




